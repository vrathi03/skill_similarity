# Import Required Libraries
import logging
import os
from datetime import datetime
from utils.config import config
from pathlib import Path

LOG_DIR_PATH = config['LOGGING_SECTION']['LOG_DIR_PATH']
logging_level = config.get('LOGGING_SECTION', 'LOG_LEVEL')

# if LOG_DIR_PATH in os.listdir():
#     pass
# else:
#     os.mkdir(LOG_DIR_PATH)
Path(LOG_DIR_PATH).mkdir(exist_ok=True)
logging.basicConfig(filename=LOG_DIR_PATH + 'skill-embedding-{:%Y-%m-%d}.log'.format(datetime.now()),
                    level=logging_level, format='%(levelname)s  %(asctime)-10s  %(message)8s',
                    datefmt='%Y-%m-%dT%H:%M:%SZ')

logger = logging.getLogger(__name__)

