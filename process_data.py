import re
import inflection as inf
import numpy as np
import pandas as pd
import logging


class LoadData(object):

    def read_skills(self, file_path):
        """
        It is a function to read the csv file
        :param file_path: file path of the csv file
        :return: Pandas DataFrame
        """
        df = pd.read_csv(file_path)

        return df

    def df_to_dict(self, df):
        """
        Creates a dictionary of df in format-> {'skill':'skill_token'}
        :param df: DataFrame containing two columns skills and skills_token
        :return: a Dictionary of format-> {'skill':'skill_token'}
        """
        try:
            skill_token_dict = dict(zip(df['skills'], df['skills_token']))
            return skill_token_dict
        except Exception as e:
            logging.error(e)
            return 'Please check datafile it should contains skills and skills_token column'


class PreProcess(object):

    def singularize_text(self, text):
        """
        Singularize the texts
        :param text: A string
        :return: Singular texts
        """
        return ' '.join([inf.singularize(item) for item in text.split()])

    def replace_occurrences(self, documents, search, skill_token_dict):
        """
        Replace space with underscore from some bigrams which occurs frequently
        :param documents:
        :param search:
        :param skill_token_dict:
        :return: replaced space from some bigrams in a text with underscore
        """
        lst = list()
        for index, document in enumerate(documents):
            text = search.sub(lambda m: skill_token_dict[re.escape(m.group(0))], document.lower())
            lst.append(text)

        return lst

    def avg_sentence_vector(self, words, model):
        """
        function to average all words vectors in a given paragraph and make a vector
        :param words:
        :param model:
        :return: A vector
        """
        doc = [word for word in words if word in model.wv.vocab]
        another_vec = np.mean(model[doc], axis=0)

        return another_vec

    def remove_non_skill_words(self, text, skill_token_list):
        """
        It removes the words from text
        :param text: A string
        :param skill_token_list: A dictionary containing skills and its tokens
        :return: A string which contains the word which are only in skill_token_list
        """
        text = text.replace(',', ' ')
        text = ' '.join([word.rstrip('.').rstrip('?').rstrip('!').strip() for word in text.split(' ') if
                         word.rstrip('.').rstrip('?').rstrip('!').strip() in skill_token_list])

        return text

    def combine_func(self, text, search, skill_token_dict, remove=True):
        """
        It returns Hashed texts
        :param text: A string
        :param search: It is a RegEx compiler object
        :param skill_token_dict: A dictionary containing skills and its tokens
        :return: hash text which are basically a string of those words in rjd or jd which are present in skill token list
        """

        skill_token_list = list(skill_token_dict.values())
        text = self.singularize_text(text)

        logging.info('Singularize text: %s', text)
        text = self.replace_occurrences([text], search, skill_token_dict)[0]

        if remove:
            text = self.remove_non_skill_words(text, skill_token_list)
            # logging.info('remove_non_skill_words text: %s', text)

        return text
