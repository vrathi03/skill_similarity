import logging
import logger
import json
import re
import en_core_web_sm
from gensim.models import Word2Vec
from sklearn.metrics.pairwise import cosine_similarity
from trie_match import Trie
from process_data import PreProcess, LoadData
from time import time
from flask import Flask, request, jsonify
from flask_restplus import Api, Resource, fields

logging.info('Starting Logging')
nlp = en_core_web_sm.load()


# Code to replace text in Most Similar Skills
def replace_str_tuple(f_str, r_str, data):
    data_list = []
    for tup in data:
        string = tup[0]
        if string.find(f_str) == -1:
            data_list.append(tup)
        else:
            string = re.sub(f_str, r_str, string)
            lst = list(tup)
            lst[0] = string
            tup = tuple(lst)
            data_list.append(tup)
    return data_list


def replace_substr(f_str, r_str, data):
    data = data[0][0]
    data_list = []
    for d in data:
        string = d['skill']
        if string.find(f_str) == -1:
            data_list.append(d)
        else:
            string = re.sub(f_str, r_str, string)
            d['skill'] = string
            data_list.append(d)
    return [[data_list]]


try:
    model = Word2Vec.load("models/embedding.model")
except Exception as e:
    logging.error(e)

logging.info('**********Model Loaded **********')


def initialize_flask():
    flask_app = Flask(__name__)
    app = Api(app=flask_app,
              version="1.0",
              title="Skill Similarity")

    se_ns = app.namespace('skill_explanations')
    tsw_ns = app.namespace('most_similar')
    gh_ns = app.namespace('get_skills')
    ts_ns = app.namespace('top_skills')
    s_ns = app.namespace('similarity')

    hashing_model = app.model('Hashing Model', {
        'text': fields.List(fields.List(fields.String)),
    })

    similarity_model = app.model('Similarity Model', {
        'text': fields.List(fields.String),
    })

    nested_fields = app.model('User', {
        'Industry': fields.String,
        'Function': fields.String,
        'hashed_description': fields.String
    })

    top_skills_model = app.model('Top Skills Model', {
        "data": fields.List(fields.List(fields.Nested(nested_fields)))
    })

    skill_similarity_model = app.model('Skill Similarity Model', {
        'jd': fields.String,
        'rjd_list': fields.List(fields.List(fields.String))
    })
    skill_explanation_model = app.model('Skill Explanation Model', {
        'jd': fields.String,
        'rjd': fields.String
    })
    name_spaces = [se_ns, gh_ns, s_ns, tsw_ns, ts_ns]

    return flask_app, app, name_spaces, hashing_model, top_skills_model, skill_similarity_model, skill_explanation_model, similarity_model


# Initialize flask/swagger api
flask_app, app, name_spaces, hashing_model, top_skills_model, skill_similarity_model, skill_explanation_model, \
similarity_model = initialize_flask()

se_ns, gh_ns, s_ns, tsw_ns, ts_ns = name_spaces

# Search strings and replacements for API responses
s1 = 'datum'
s2 = 'sales_operation'
r1 = 'data'
r2 = 'sales operations'


@tsw_ns.route('')
class TopSimilar(Resource):
    @app.expect(similarity_model)
    def post(self):
        """
        Takes input as list of words
        :return: A dictionary of similar words and there score
        """
        try:
            data = json.loads(request.get_data().decode('utf-8'), strict=False)
            text = data['text']
            text = [t.replace("_", " ").lower() for t in text]

            sub_hash = list()
            for new_text in text:
                try:
                    hash_text = prep_process.combine_func(str(new_text), search, skill_token_dict)
                    hash_text = ' '.join(list(set(hash_text.split(' '))))  # Keeping only unique elements
                    print("HASH TEXT:", hash_text)
                except Exception as e:
                    logging.info("{} not found in hash".format(new_text))
                    hash_text = ''
                sub_hash.append(hash_text)
        except Exception as e:
            logging.error(e)
            return "Please provide correct input data!, Hint: Please check there is not extra comma's in list provided"

        simililar_list = list()
        for word in sub_hash:
            try:
                similar = model.wv.most_similar(word, topn=10)
                # Hard coded replacement of datum with data; to be reverted
                similar = replace_str_tuple(s1, r1, similar)
                similar = replace_str_tuple(s2, r2, similar)

                print("SIMILAR:", similar)
            except Exception as e:
                logging.error(e)
                similar = []
            simililar_list.append(similar)
            logging.info('list: {}'.format(simililar_list))

        return jsonify(simililar_list)


@gh_ns.route('')
class Hashing(Resource):
    @app.expect(hashing_model)
    def post(self):
        """
        Takes Input as text and gives hashed text as ouput
        :return: hashed text
        """
        st = time()
        logging.info('********** Hashing Function Started **********')

        try:
            data = json.loads(request.get_data().decode('utf-8'), strict=False)
            text = data['text']

        except Exception as e:
            logging.error(e)
            return 'Please provide correct input data!'

        hashed_texts = list()
        for ls in text:
            sub_hash = list()
            for new_text in ls:
                try:
                    hash_text = prep_process.combine_func(str(new_text), search, skill_token_dict)
                    hash_text = ' '.join(list(set(hash_text.split(' '))))  # Keeping only unique elements
                except Exception as e:
                    logging.info("{} not found in hash".format(new_text))
                    hash_text = ''
                sub_hash.append(hash_text)
            hashed_texts.append(sub_hash)

        logging.info('Sending hashed output back, total time taken: %s', time() - st)
        logging.info('**********Hashing Function Ended**********')

        return jsonify({"Hashed text": hashed_texts})


@ts_ns.route('')
class Top_skills(Resource):
    @app.expect(top_skills_model)
    def post(self):
        """
        Generates top skills
        :return: List of top scores
        """
        st = time()
        logging.info('**********Top Skills Function Started**********')

        try:
            response = json.loads(request.get_data().decode('utf-8'), strict=False)
            data_object = response['data']
        except Exception as e:
            logging.error(e)
            return 'Please provide correct input data!'

        resume_list = list()
        for resume in data_object:
            resume_score_list = list()

            for data in resume:
                try:
                    # Replacing underscore with space - on demand change
                    ind = ' '.join([indy.lower().replace("_", " ") for indy in data['Industry'].split(' ')])
                    fun = ' '.join([indy.lower().replace("_", " ") for indy in data['Function'].split(' ')])
                    industry_hash = prep_process.combine_func(ind, search,
                                                              skill_token_dict,
                                                              remove=False)
                    function_hash = prep_process.combine_func(fun, search,
                                                              skill_token_dict,
                                                              remove=False)

                    hashed_skills = data['hashed_description'].lower()

                    ind_func_hash = industry_hash + ' ' + function_hash
                    logging.info("Industry Function Hash : {}".format(ind_func_hash))

                except Exception as e:
                    logging.error(e)
                    return 'Something went wrong while processing the input data.'

                if not any([ind in model.wv.vocab for ind in industry_hash.split(' ')]) and not any([
                    func in model.wv.vocab for func in function_hash.split(' ')]):
                    resume_score_list.append([])

                    continue  # Moving to next entry

                ind_func_vec = prep_process.avg_sentence_vector(ind_func_hash.lower().split(), model=model)
                direct_score_list = list()

                for skill in hashed_skills.split(' '):
                    direct_score_dict = dict()
                    direct_score_dict['skill'] = skill

                    if skill not in model.wv.vocab:
                        direct_sim = float(0.0)
                    else:
                        skill_vec = prep_process.avg_sentence_vector([skill.lower()], model=model)
                        direct_sim = float(cosine_similarity([ind_func_vec], [skill_vec])[0][0])

                    direct_score_dict['skill_score'] = float(direct_sim)
                    direct_score_list.append(direct_score_dict)

                direct_score_list = [dict(t) for t in {tuple(d.items()) for d in direct_score_list}]
                direct_score_list = sorted(direct_score_list, key=lambda i: i['skill_score'], reverse=True)

                # Adding useless logic here #1
                total_items = len(direct_score_list)
                # limit = 0.4
                # items_to_return = int(total_items*limit)
                logging.info("total number of skills identified : {}".format(total_items))
                # logging.info("total number of skills returned : {}".format(items_to_return))
                # if direct_score_list:
                #     direct_score_list = direct_score_list[:items_to_return]

                # Adding useless logic here #2
                direct_score_list = [element for element in direct_score_list if element['skill_score'] >= 0.4]
                logging.info("total number of skills returned : {}".format(len(direct_score_list)))
                resume_score_list.append(direct_score_list)
            logging.info("resume score list in top skills : {}".format(resume_score_list))

            resume_list.append(resume_score_list)

        # Hard coded replacement of datum with data; to be reverted
        resume_list = replace_substr(s1, r1, resume_list)
        resume_list = replace_substr(s2, r2, resume_list)

        logging.info('Generated top skills and sent, total time taken: %s', time() - st)
        logging.info('**********Top Skills Function Ended**********')

        return jsonify(resume_list)


@s_ns.route('')
class Similarity(Resource):

    @app.expect(skill_similarity_model)
    def post(self):
        """
        Calculates similarity between jd and rjd list
        :return: list of similarity scores
        """
        st = time()
        logging.info('**********Similarity Function Started**********')

        try:
            data = json.loads(request.get_data().decode('utf-8'), strict=False)
            hashed_jd = data['jd']
            hashed_rjd_list = data['rjd_list']  # List of list of text
            if hashed_jd == "" or hashed_rjd_list[0][0] == "":
                return jsonify([[0.0]])
        except Exception as e:
            logging.error(e)
            logging.error('similarity')

            return 'Please provide correct data'

        logging.info('Vectorizing it and calculating score')
        try:
            hashed_jd = hashed_jd.replace("_", " ").lower()
            hashed_jd = prep_process.combine_func(hashed_jd, search, skill_token_dict, remove=False)
            logging.info("Hashed JD : {}".format(hashed_jd))
            if not any([ind in model.wv.vocab for ind in hashed_jd.strip().split(' ')]):
                logging.info("No word in JD found in Embeddings")
                return jsonify([[0.0]])
            jd_vec = prep_process.avg_sentence_vector(hashed_jd.lower().split(), model=model)

        except Exception as e:
            logging.error(e)

            return 'Provide correct Input'

        hashed_rjd_score_list = []
        for resume in hashed_rjd_list:
            resume_score_list = []
            for hashed_rjd in resume:
                hashed_rjd = str(hashed_rjd)
                hashed_rjd = hashed_rjd.replace("_", " ").lower()
                hashed_rjd = prep_process.combine_func(hashed_rjd, search, skill_token_dict, remove=False)
                if not any([ind in model.wv.vocab for ind in hashed_rjd.strip().split(' ')]):
                    resume_score_list.append([])
                    continue  # Moving to next entry
                try:

                    logging.info("Hashed RJD : {}".format(hashed_rjd))
                    rjd_vec = prep_process.avg_sentence_vector(hashed_rjd.lower().split(), model=model)
                except Exception as e:
                    logging.error(e)
                    return e

                full_score = float(cosine_similarity([jd_vec], [rjd_vec])[0][0])
                resume_score_list.append(full_score)

            hashed_rjd_score_list.append(resume_score_list)

        logging.info('Sending result back, total time taken: %s', time() - st)
        logging.info('**********Similarity Function Ended**********')

        return jsonify(hashed_rjd_score_list)


@se_ns.route('')
class SkillExplanation(Resource):
    @app.expect(skill_explanation_model)
    def post(self):
        """
        Calculates Impact Score and Score without skills
        :return: A dictionary containing dictionaries of score
        """
        st = time()
        logging.info('**********Skill Explanation Started***********')

        try:
            data = json.loads(request.get_data().decode('utf-8'), strict=False)
            jd = data['jd']
            rjd = data['rjd']

        except Exception as e:
            logging.error(e)

            return "Bad request", 400

        hashed_jd = prep_process.combine_func(jd, search, skill_token_dict)
        hashed_rjd = prep_process.combine_func(rjd, search, skill_token_dict)

        try:
            jd_vec = prep_process.avg_sentence_vector(hashed_jd.lower().split(), model=model)

        except Exception as e:
            logging.info('hashed jd; %s', hashed_jd)
            logging.info('hashed rjd; %s', hashed_rjd)
            logging.error(e)

            return 'Please provide correct data'

        rjd_vec = prep_process.avg_sentence_vector(hashed_rjd.lower().split(), model=model)

        score_list = list()

        full_score = cosine_similarity([jd_vec], [rjd_vec])[0][0]

        for skill in hashed_rjd.split(' '):
            score_dict = dict()
            score_dict['skill'] = skill
            temp_text = hashed_rjd.replace(skill, '').strip()
            try:
                temp_vec = prep_process.avg_sentence_vector(temp_text.lower().split(), model=model)
            except Exception as e:
                logging.error(e)
                score_dict['impact'] = float(0)
                score_dict['score_without_skill'] = str(0)
                continue

            sim = cosine_similarity([jd_vec], [temp_vec])[0][0]

            score = full_score - sim

            score_dict['impact'] = float(score)
            score_dict['score_without_skill'] = str(sim)

            score_list.append(score_dict)

        sent_score_list = []
        sent_list = list(nlp(rjd).sents)

        logging.info("sent list {}".format(sent_list))
        logging.info("Length sent list {}".format(len(sent_list)))

        if sent_list:
            for sent in sent_list:
                score_dict = dict()
                score_dict['sentence'] = str(sent)
                temp_text = rjd.replace(str(sent), '').strip()
                temp_text = prep_process.combine_func(temp_text, search, skill_token_dict)

                try:
                    temp_vec = prep_process.avg_sentence_vector(temp_text.lower().split(), model=model)

                except Exception as e:
                    logging.error(e)
                    score_dict['impact'] = float(full_score)
                    score_dict['score_without_sentence'] = str(0)
                    sent_score_list.append(score_dict)

                    continue

                sim = cosine_similarity([jd_vec], [temp_vec])[0][0]
                score = full_score - sim
                score_dict['impact'] = float(score)
                score_dict['score_without_sentence'] = str(sim)

                sent_score_list.append(score_dict)

        else:
            sent_score_list = []

        response = dict()
        response['Full Score'] = str(full_score)

        response['Sentence impact'] = sorted(sent_score_list, key=lambda i: i['impact'], reverse=True)
        score_list = [dict(t) for t in {tuple(d.items()) for d in score_list}]
        response['Skill impact'] = sorted(score_list, key=lambda i: i['impact'], reverse=True)

        # logging.info('response type: %s', type(jsonify(response)))
        logging.info('Time taken in the whole function: %s', time() - st)
        logging.info('**********Skill Explanation Ended***********')

        return jsonify(response)


# Initializing globally
prep_process = PreProcess()
load_data = LoadData()

skill_token_list_path = r"mixed_main_skills_token_list.csv"
df = load_data.read_skills(skill_token_list_path)

skill_token_dict = load_data.df_to_dict(df)

trie = Trie()

terms = df['skills']
terms = sorted(terms, key=len, reverse=True)

for word in terms:
    trie.add(word)

search = re.compile(r"\b" + trie.pattern() + r"\b", re.IGNORECASE)

skill_token_dict = dict((re.escape(k), v) for k, v in skill_token_dict.items())

if __name__ == '__main__':
    flask_app.run(host='0.0.0.0', port=5000, debug=True)
