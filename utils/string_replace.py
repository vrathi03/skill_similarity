#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

Created on 2019-07-05 17:40

@author: Vaibhav Rathi

"""

# Import Required Libraries
import re
# find_str = 'datum'
# repl_str = 'data'


def replace_substr(f_str, r_str, data):
    data = data[0][0]
    data_list = []
    for d in data:
        string = d['skill']
        if string.find(f_str) == -1:
            data_list.append(d)
        else:
            print("YES")
            string = re.sub(f_str, r_str, string)
            print("replaced:", string)
            d['skill'] = string
            data_list.append(d)
    return [[data_list]]
